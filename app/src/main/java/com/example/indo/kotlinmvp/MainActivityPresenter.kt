package com.example.indo.kotlinmvp


class MainActivityPresenter (_view: ContractInterface.View): ContractInterface.Presenter {
    override fun incrementvalue() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        model.IncrementCounter()
        view.UpdateView()
    }

    override fun getCount()= model.getCounter().toString()

    private var view : ContractInterface.View = _view
    private var model : ContractInterface.Model = MainActivityModel()

     init{
        view.initView()
    }

}