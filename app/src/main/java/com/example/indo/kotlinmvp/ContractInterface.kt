package com.example.indo.kotlinmvp

interface ContractInterface {

    interface  View{
        fun initView()
        fun UpdateView()
    }
    interface Presenter{
        fun incrementvalue()
        fun getCount():String
    }
    interface Model{
        fun getCounter(): Int
        fun IncrementCounter()
    }
}