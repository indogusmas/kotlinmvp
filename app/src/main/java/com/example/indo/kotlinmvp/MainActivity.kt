package com.example.indo.kotlinmvp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), ContractInterface.View {

    private var presenter: MainActivityPresenter ? = null

    override fun initView() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        counterTextView.text = presenter?.getCount()
        clickButton.setOnClickListener{presenter?.incrementvalue()}

    }

    override fun UpdateView() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        counterTextView.text = presenter?.getCount()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = MainActivityPresenter(this);
    }
}
